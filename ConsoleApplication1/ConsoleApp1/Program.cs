﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Program
    {
        
        private static readonly string[] cities = new string[19]
        {
            "Київ",
            "Житомир",
            "Новоград-Волинський",
            "Рівне",
            "Луцьк",
            "Бердичів",
            "Вінниця",
            "Хмельницький",
            "Тернопіль",
            "Шепетівка",
            "Білацерква",
            "Умань",
            "Черкаси",
            "Кременчуг",
            "Полтава",
            "Харків",
            "Прилуки",
            "Суми",
            "Миргород"
        };

        
        private static readonly int[,] matrix = new int[19, 19];

        //в ширину
        private static void Bfs()
        {
            int[] route = new int[20];
            bool[] visited = new bool[20];
            int cityIndex = 0;
            visited[cityIndex] = true;
            Queue<int> queue = new Queue<int>(); // Виправлено тип черги
            queue.Enqueue(cityIndex);
            while (queue.Count != 0)
            {
                // Дістаємо місто для перевірки
                cityIndex = queue.Dequeue();
                for (int i = 0; i < cities.Length; i++)
                {
                    // Якщо знайдений маршрут і місто не відвідане
                    if (matrix[cityIndex, i] != 0 && !visited[i])
                    {
                        // Відмічаємо, що були в цьому місті
                        visited[i] = true;
                        // Вносимо наступне місце у чергу
                        queue.Enqueue(i);
                        // Рахуємо кілометраж
                        route[i] = route[cityIndex] + matrix[cityIndex, i];
                        Console.WriteLine($"Київ - {cities[i]} ({route[i]}км)");
                    }
                }
            }
        }

        static int path = 0;

        //  глибина
        private static void Dfs(int cityIndex, string route, int cityForOutput)
        {
            // Додавання міста
            if (cityIndex != 0)
            {
                route = $"{route} - {cities[cityIndex]}";
                // Додаємо кілометраж
                path += matrix[cityForOutput, cityIndex];
                Console.WriteLine($"{route} ({path}км)");
            }
            // Початкове місто
            else
            {
                route = $"{cities[cityIndex]}";
            }
            // Рекурсія пошуку в глибину
            for (int i = 0; i < cities.Length; i++)
            {
                if (matrix[cityIndex, i] != 0)
                {
                    Dfs(i, route, cityIndex);
                    path -= matrix[cityIndex, i];
                }
            }
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            matrix[0, 1] = 135;
            matrix[1, 2] = 80;
            matrix[2, 3] = 100;
            matrix[3, 4] = 68;
            matrix[1, 5] = 38;
            matrix[5, 6] = 73;
            matrix[6, 7] = 110;
            matrix[7, 8] = 104;
            matrix[1, 9] = 115;
            matrix[0, 10] = 78;
            matrix[10, 11] = 115;
            matrix[10, 12] = 146;
            matrix[12, 13] = 105;
            matrix[10, 14] = 181;
            matrix[14, 15] = 130;
            matrix[0, 16] = 128;
            matrix[16, 17] = 175;
            matrix[16, 18] = 109;
            Console.WriteLine("\n ---------- Пошук у ширину ----------\n ");
            Bfs();
            int cityIndex = 0;
            string route = "";
            Console.WriteLine();
            Console.WriteLine("\n---------- Пошук у глибину ----------\n ");
            Dfs(cityIndex, route, 0);
            Console.WriteLine("\n");
        }
    }
}
